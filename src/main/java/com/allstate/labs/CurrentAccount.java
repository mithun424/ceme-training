package com.allstate.labs;

public class CurrentAccount extends Account {
    
    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.1);
    }

    public CurrentAccount() {
    }

    public CurrentAccount(double balance, String name) {
        super(balance, name);
    }
}
