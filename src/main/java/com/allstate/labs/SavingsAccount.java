package com.allstate.labs;

public class SavingsAccount extends Account {
    
    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.1);
    }

    public SavingsAccount() {
    }

    public SavingsAccount(double balance, String name) {
        super(balance, name);
    }
}
