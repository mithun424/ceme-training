package com.allstate.labs;

public abstract class Account {
    private double balance;
    private String name;
    private static double interestRate = 0.10;
    
    public Account() {
        this.name = "TestName";
        this.balance = 50;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
       double outstanding = this.balance - amount;
       if(outstanding > 0) {
           this.balance = outstanding;
            return true;
       }
       return false;
    }

    public boolean withdraw() {
        return withdraw(20);
    }
}
