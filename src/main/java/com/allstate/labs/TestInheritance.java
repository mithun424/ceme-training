package com.allstate.labs;

public class TestInheritance {
    public static void main(String[] args) {
        double[] amounts = {2,4,6};
        String[] names = {"Picard", "Ryker", "Worf"};

        Account[] arrayOfAccounts = new Account[amounts.length];
        
        arrayOfAccounts[0] = new CurrentAccount(amounts[0], names[0]);
        System.out.println("Name is " + arrayOfAccounts[0].getName());
        System.out.println("Balance is " + arrayOfAccounts[0].getBalance());

        arrayOfAccounts[0].addInterest();

        System.out.println("Balance is " + arrayOfAccounts[0].getBalance());   

        arrayOfAccounts[1] = new SavingsAccount(amounts[1], names[1]);
        System.out.println("Name is " + arrayOfAccounts[1].getName());
        System.out.println("Balance is " + arrayOfAccounts[1].getBalance());

        arrayOfAccounts[1].addInterest();

        System.out.println("Balance is " + arrayOfAccounts[1].getBalance());   

        arrayOfAccounts[2] = new CurrentAccount(amounts[2], names[2]);
        System.out.println("Name is " + arrayOfAccounts[2].getName());
        System.out.println("Balance is " + arrayOfAccounts[2].getBalance());

        arrayOfAccounts[2].addInterest();

        System.out.println("Balance is " + arrayOfAccounts[2].getBalance());     
    }
}
