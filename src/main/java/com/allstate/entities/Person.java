package com.allstate.entities;

public class Person {
    private int id;
    private String name;
    private int age;
    // quiz final - its like constant, sealing (can't subclass), can't method overriding
    // private final static int ageLimit = 100;
    private static int ageLimit = 100;
    
    //default
    public Person() {
        this.id = 0;
    }

    // Assign static values
    // static {
    //     ageLimit = 105;
    // }

    public static int getAgeLimit() {
        return ageLimit;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void display() {
        System.out.printf("Values are %s, %d", this.name, this.id);
    }

    // protected void print() {
    //     System.out.printf("Values are %s, %d", this.name, this.id);
    // }

    // void printf() {
    //     System.out.printf("Values are %s, %d", this.name, this.id);
    // }

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
