package com.allstate.lambdademo;

import java.util.function.*;

public class TestLambdas {
    public static void main(String[] args) {
        GreetingServiceImpl greetingServiceImpl = new GreetingServiceImpl();
        greetingServiceImpl.sayMessage("Mithun");

        //Using Lambda
        GreetingService greatService = (s) -> {
            System.out.println("Hello " + s);
        };

        greatService.sayMessage("Mithun");

        // parameter and return value
        Function<Integer, String> myFunc = (a) -> {
            System.out.println(a);
            return "finished";
        };
        String output = myFunc.apply(1);
        System.out.println(output);

        // returns true or false
        Predicate<String> onlyReturnsTrueOrFalse = (s) -> true;
        onlyReturnsTrueOrFalse.test("t");

        // takes a parameter no return value
        Consumer<Integer> myCons = (a) -> System.out.println(a);
        Consumer<Integer> myCons2 = System.out::println;
        myCons.accept(1);
        myCons2.accept(2);

        // returns a value but doesn’t take a value
        IntSupplier mySupp = () -> {return 5+5;};
        int res = mySupp.getAsInt();
        System.out.println(res);

        
    }
}
