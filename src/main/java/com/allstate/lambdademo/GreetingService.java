package com.allstate.lambdademo;

public interface GreetingService {
    void sayMessage(String message);
}
