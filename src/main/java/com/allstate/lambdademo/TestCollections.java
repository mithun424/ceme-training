package com.allstate.lambdademo;

import java.util.*;

import com.allstate.entities.Employee;

public class TestCollections {

  public static void main(String[] args) {
      List<Employee> employees = new ArrayList<Employee>();
      Employee e1 = new Employee(1, "MM", 30 , "rms", 2000);
      Employee e2 = new Employee(2, "AA", 30 , "rms", 2000);
      Employee e3 = new Employee(3, "MM", 30 , "rms", 2000);

      employees.add(e1);
      employees.add(e2);
      employees.add(e3);
      for (Employee employee : employees) {
          employee.display();
      }

      employees.stream().filter(p -> p.getName() == "MM" && p.getAge() >= 18)
      .map(p -> p.getEmail())
      .forEach(email -> System.out.println(email));

      Set<Employee> employees2 = new TreeSet<Employee>((p1, p2) -> p1.getName().compareTo(p2.getName()));

      employees2.add(e1);
      employees2.add(e2);
      employees2.add(e3);
      for (Employee employee : employees2) {
        employee.display();
      }
      
  }  
}
